export interface IMoviePreview {
  title: string;
  slug: string;
  preview_image_url: string;
  summary: string;
  year?: number;
}
