export interface ISignUpResponse {
  message?: string;
  error?: string;
}
