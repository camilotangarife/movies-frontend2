import { Component, OnInit } from '@angular/core';
import {IMoviePreview} from '../../interfaces/movie-preview.interface';
import {MoviesApiService} from '../../services/movies-api.service';
import {IGetMoviesResponse} from '../../interfaces/get-movies-response.interface';
import {MatDialog, PageEvent} from '@angular/material';
import {CustomDialogComponent} from '../custom-dialog/custom-dialog.component';

@Component({
  selector: 'app-my-movies',
  templateUrl: './my-movies.component.html',
  styleUrls: []
})
export class MyMoviesComponent implements OnInit {
  public page = 0;
  public totalMovies = 0;
  public finished = false;
  public movies: Array<IMoviePreview> = [];
  constructor(private moviesApi: MoviesApiService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getMyMovies();
  }

  private getMyMovies() {
    this.finished = false;
    this.movies = [];
    this.moviesApi.getMyMovies(this.page).subscribe((response: IGetMoviesResponse) => {
      this.movies = response.movies;
      this.totalMovies = response.total_movies;
      this.finished = true;
    });
  }

  public deleteMovie(slug: string) {
    this.moviesApi.deleteMovie(slug).subscribe(
      (response) => {
        this.dialog.open(CustomDialogComponent, {
          data: {
            title: 'The movie was deleted',
            message: ''
          }
        });
        this.getMyMovies();
      },
      err => {
        this.dialog.open(CustomDialogComponent, {
          data: {
            title: 'Error',
            message: err.error.error
          }
        });
      }
    );
  }

  public pageChanged(pageEvent: PageEvent) {
    this.page = pageEvent.pageIndex;
    this.getMyMovies();
  }
}
