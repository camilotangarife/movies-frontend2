import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MoviesApiService} from '../../services/movies-api.service';
import {CustomDialogComponent} from '../custom-dialog/custom-dialog.component';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-create-movie',
  templateUrl: './create-movie.component.html',
  styleUrls: []
})
export class CreateMovieComponent implements OnInit {
  public createMovieFormGroup: FormGroup;
  private formData = new FormData();
  constructor(private moviesApi: MoviesApiService, private cd: ChangeDetectorRef, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.createMovieFormGroup = new FormGroup({
      title: new FormControl('', [Validators.required]),
      preview_image: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
      year: new FormControl('', [Validators.required, Validators.min(1800), Validators.max(2020)]),
      summary: new FormControl('', [Validators.required]),

      director: new FormControl(),
      writers: new FormControl(),
      stars: new FormControl('', [Validators.min(1), Validators.max(5)]),
      actors_actresses: new FormControl(),
      trailer: new FormControl()
    });
  }

  public hasError(controlName: string, errorName: string) {
    return this.createMovieFormGroup.controls[controlName].hasError(errorName);
  }

  public onFileChange(event, fileId: string) {
    if (event.target.files && event.target.files.length) {
      const value = {};
      value[fileId] = event.target.files[0];
      this.createMovieFormGroup.patchValue(value);
      this.cd.markForCheck();
    }
  }

  public createMovie() {
    if (this.createMovieFormGroup.valid) {
      const formValue = this.createMovieFormGroup.value;
      this.formData.append('title', formValue.title);
      this.formData.append('preview_image', formValue.preview_image);
      this.formData.append('image', formValue.image);
      this.formData.append('year', formValue.year);
      this.formData.append('summary', formValue.summary);
      this.formData.append('director', formValue.director);
      this.formData.append('writers', formValue.writers);
      this.formData.append('stars', formValue.stars);
      this.formData.append('trailer', formValue.trailer);
      this.moviesApi.createMovie(this.formData).subscribe(
        (response: boolean) => {
          const dialogRef = this.dialog.open(CustomDialogComponent, {
            data: {
              title: 'Your movie was created',
              message: ''
            }
          });
          dialogRef.afterClosed().subscribe(result => {
            this.router.navigate(['/my-movies']);
          });
        },
        err => {
          this.dialog.open(CustomDialogComponent, {
            data: {
              title: 'Error',
              message: err.error.error
            }
          });
        }
      );
    }
  }
}
