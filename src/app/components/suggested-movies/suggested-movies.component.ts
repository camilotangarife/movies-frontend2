import { Component, OnInit } from '@angular/core';
import {IMoviePreview} from '../../interfaces/movie-preview.interface';
import {MoviesApiService} from '../../services/movies-api.service';
import {IGetMoviesResponse} from '../../interfaces/get-movies-response.interface';
import {MatDialog, PageEvent} from '@angular/material';
import {CustomDialogComponent} from '../custom-dialog/custom-dialog.component';

@Component({
  selector: 'app-suggested-movies',
  templateUrl: './suggested-movies.component.html',
  styleUrls: []
})
export class SuggestedMoviesComponent implements OnInit {
  public page = 0;
  public totalMovies = 0;
  public finished = false;
  public movies: Array<IMoviePreview> = [];
  constructor(private moviesApi: MoviesApiService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getSuggestedMovies();
  }

  private getSuggestedMovies() {
    this.finished = false;
    this.movies = [];
    this.moviesApi.getSuggestedMovies(this.page).subscribe((response: IGetMoviesResponse) => {
      this.movies = response.movies;
      this.totalMovies = response.total_movies;
      this.finished = true;
    }, err => {
      this.finished = true;
      this.dialog.open(CustomDialogComponent, {
        data: {
          title: 'Error',
          message: 'Please try again'
        }
      });
    });
  }

  public pageChanged(pageEvent: PageEvent) {
    this.page = pageEvent.pageIndex;
    this.getSuggestedMovies();
  }
}
