import { Component, OnInit } from '@angular/core';
import {MoviesApiService} from '../../services/movies-api.service';
import {CustomDialogComponent} from '../custom-dialog/custom-dialog.component';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: []
})
export class NavigationComponent implements OnInit {

  constructor(private moviesApi: MoviesApiService, private dialog: MatDialog, private router: Router) { }

  ngOnInit() {
  }

  public isLogged(): boolean {
    const token = MoviesApiService.getToken();
    return token && token !== '';
  }

  public logout() {
    MoviesApiService.deleteToken();
    const dialogRef = this.dialog.open(CustomDialogComponent, {
      data: {
        title: 'Logout succeed',
        message: ''
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.moviesApi.logOut().subscribe(
        (response: boolean) => {
          this.router.navigate(['/movies']);
        },
        () => {
          this.router.navigate(['/movies']);
        }
      );
    });
  }
}
