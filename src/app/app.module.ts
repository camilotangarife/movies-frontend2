import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import {
  MatDatepickerModule, MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MoviesListComponent } from './components/movies-list/movies-list.component';
import { MoviePreviewComponent } from './components/movie-preview/movie-preview.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SignupComponent } from './components/signup/signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { HttpClientModule } from '@angular/common/http';
import { CustomDialogComponent } from './components/custom-dialog/custom-dialog.component';
import { SuggestedMoviesComponent } from './components/suggested-movies/suggested-movies.component';
import { MyMoviesComponent } from './components/my-movies/my-movies.component';
import { CreateMovieComponent } from './components/create-movie/create-movie.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavigationComponent,
    MoviesListComponent,
    MoviePreviewComponent,
    PageNotFoundComponent,
    SignupComponent,
    MovieDetailsComponent,
    CustomDialogComponent,
    SuggestedMoviesComponent,
    MyMoviesComponent,
    CreateMovieComponent
  ],
  entryComponents: [
    CustomDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
